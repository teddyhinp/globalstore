import stepStore, { stepActions } from '../../globalStore/step';
import useStore from '../../globalStore/useStore';

export default function Home() {
    return (
        <div style={style}>
            <StepOne />
            <StepTwo />
        </div>
    );
}

function StepOne() {
    let step1 = useStore(stepStore, 'step1');
    const handleAdd1 = () => {
        stepActions.addStep1();
    };
    const handleMinus1 = () => {
        stepActions.minusStep1();
    };
    return (
        <div>
            step:
            {step1}
            <div>
                <button onClick={handleAdd1}>Add Step</button>
                <button onClick={handleMinus1}>Minus Step</button>
            </div>
        </div>
    );
}

function StepTwo() {
    let step2 = useStore(stepStore, 'step2');
    const handleAdd2 = () => {
        stepActions.addStep2();
    };
    const handleMinus2 = () => {
        stepActions.minusStep2();
    };
    return (
        <div>
            step2:
            {step2}
            <div>
                <button onClick={handleAdd2}>Add Step</button>
                <button onClick={handleMinus2}>Minus Step</button>
            </div>
        </div>
    );
}

const style = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    gap: '20px',
};
