import React, { useState, useEffect } from 'react';

export function CreateStore(initState) {
    let currentState = initState;
    const listeners = new Set();
    return {
        state: (filter) => (!filter ? currentState : currentState[filter]),
        setState: (newValue) => {
            currentState = newValue;
            listeners.forEach((listener) => listener(currentState));
        },
        subscribe: (listener) => {
            listeners.add(listener);
            return () => listeners.delete(listener);
        },
    };
}

function Destructing(state, destructor) {
    try {
        if (!destructor) return state;
        if (typeof destructor != 'string') throw 'destructor must be string';
        const [currDestructor, ...restDestructor] = destructor.split('.');
        if (restDestructor.length === 0) return state[currDestructor];
        return Destructing(state[currDestructor], restDestructor.join('.'));
    } catch (error) {
        console.log('UseStore destructing Error', error.message);
        return 'UseStore destructing Error' + error.message;
    }
}

const useStore = (store = { state: () => {}, setState: () => {} }, stateFilter = '') => {
    const [state, setState] = useState(Destructing(store.state(), stateFilter));
    useEffect(() => store.subscribe((state) => setState(Destructing(state, stateFilter))), []);
    return state;
};

export default useStore;
