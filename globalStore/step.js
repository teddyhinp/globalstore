import { CreateStore } from './useStore';

export const initValues = {
    step1: 0,
    step2: 0,
};

const store = CreateStore(initValues);

function Actions() {
    const updateStore = (state, value) => {
        store.setState({ ...store.state(), [state]: value });
    };

    const addStep1 = () => {
        updateStore('step1', store.state('step1') + 1);
    };
    const minusStep1 = () => {
        updateStore('step1', store.state('step1') - 1);
    };

    const addStep2 = () => {
        updateStore('step2', store.state('step2') + 1);
    };
    const minusStep2 = () => {
        updateStore('step2', store.state('step2') - 1);
    };

    return {
        addStep1,
        minusStep1,
        addStep2,
        minusStep2,
    };
}

export const stepActions = Actions();
export default store;
